# Sistem Informasi Perpustakaan
* Aplikasi perpustakaan berbasis web ini memiliki user login dengan role admin dan petugas.
* Untuk role admin mmiliki akses Create, Read, Update, Delete (CRUD) ke semua data sedangkan petugas hanya bisa melayani peminjaman dan pengembalian buku ke masing-masing anggota.
* Aplikasi ini memiliki fitur notifikasi yang mana admin bisa mengirim notifikasi ke semua petugas yang melayani peminjaman dan pengembalian buku.
